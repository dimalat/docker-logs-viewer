using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using Npgsql;
using System.Linq;

namespace logger.Data
{
    public class DataBase
    {
        private static string connectionString =
            "User ID=postgres;Password=qwerty;Host=localhost;Port=5432;Database=logger;Connection Lifetime=0;";
        public async Task<List<UserSettings>> GetUserSettings(string userName = null)
        {
            await using var conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();

            var command = @"
                select ""u"".""Username"", ""s"".""Name"", ""us"".""Value"" 
                from ""UserSettings"" us, ""Users"" u, ""Settings"" s
                where ""u"".""Username"" = ""us"".""Username"" and ""s"".""Name"" = ""us"".""SettingName""";

            if (userName != null)
                command += $"and \"u\".\"Username\" = '{userName}'";

            using var cmd = new NpgsqlCommand(command, conn);
            using var reader = await cmd.ExecuteReaderAsync();

            var result = new List<UserSettings>();
            while (await reader.ReadAsync())
            {
                var s = new UserSettings();
                s.Username = reader.GetString(0);
                s.SettingName = reader.GetString(1);
                s.Value = reader.GetString(2);
                result.Add(s);
            }

            return result;
        }

        public async Task StoreSetting(string value, string settingName, string username)
        {
            var existingUserSettings = await GetUserSettings(username);

            var alreadyExists = existingUserSettings.Any(x => x.SettingName == settingName);

            await using var conn = new NpgsqlConnection(connectionString);
            await conn.OpenAsync();


            string command = null;
            using var cmd = new NpgsqlCommand(null, conn);

            if (!alreadyExists)
            {
                command = @"INSERT INTO ""UserSettings"" (""SettingName"", ""Username"", ""Value"")
                                VALUES(@settingName, @userName, @value);";
            }
            else
            {
                command = @"
                UPDATE ""UserSettings"" SET
                ""Value"" = @value 
                WHERE ""Username"" = @userName AND ""SettingName"" = @settingName
                ";
            }

            cmd.Parameters.AddWithValue("value", value);
            cmd.Parameters.AddWithValue("settingName", settingName);
            cmd.Parameters.AddWithValue("userName", username);

            cmd.CommandText = command;
            await cmd.ExecuteNonQueryAsync();
        }
    }

    public class UserSettings
    {
        public string Username { get; set; }
        public string SettingName { get; set; }
        public string Value { get; set; }
    }
}