using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dimalat.RemoteDockerLogsFetcher;

namespace logger.Data
{
    public class LogsService
    {
        private RemoteCommandExecutor remoteCommandExecutor;
        private DockerLogsFetcher fetcher;
        private List<string> processNames;

        public LogsService()
        {
            var user = Environment.GetEnvironmentVariable("ecom_user");
            var pass = Environment.GetEnvironmentVariable("ecom_pass");
            var server = Environment.GetEnvironmentVariable("ecom_server");

            if (user != null)
            {
                remoteCommandExecutor = new RemoteCommandExecutor(server, user, pass);
                fetcher = new DockerLogsFetcher(remoteCommandExecutor);
            }
        }

        public async Task<List<string>> GetProcessNames()
        {
            if (remoteCommandExecutor == null)
                return FakeProcessNames();

            if (processNames != null)
                return processNames;

            var dockerPs = await remoteCommandExecutor
                                        .Execute("docker ps");

            var processLines = dockerPs.Split(Environment.NewLine);
            processNames = processLines.Select(x => x.Split(' ').Last().Replace("\r", string.Empty)).SkipLast(1).Skip(1).ToList();

            return processNames;
        }

        private List<string> FakeProcessNames()
        {
            return new List<string> { "Process 1", "Process 2", "Process 3" };
        }

        public async Task<List<Log>> GetLastLogs(int fetchCount, string processName = null)
        {
            processName = processName ?? await GetDefaultProcessName();

            if (remoteCommandExecutor != null)
                return new LogParser().Parse(await fetcher.Get(processName, fetchCount));

            // fake mode
            return GenerateFakeLogs(fetchCount);
        }

        private async Task<string> GetDefaultProcessName()
        {
            return (await GetProcessNames()).FirstOrDefault();
        }

        private List<Log> GenerateFakeLogs(int fetchCount)
        {
            var possibleLogsMessages = new List<string> {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam maximus, sapien eget tincidunt malesuada",
                "scelerisque tortor velit fermentum augue. Proin iaculis ligula in dignissim luctus. Quisque mollis erat",
                "Nam ligula metus, faucibus nec nibh ac, gravida convallis neque. Curabitur ",
                "Mauris maximus magna nec sodales aliquet. Nullam mattis, nisi non posuere tristique, eli",
                "si. Ut aliquet bibendum tempus. Suspendisse finibus, massa id scelerisque malesuada, lacus",
                "amet mollis luctus, urna quam placerat lectus, in consequat ipsum diam vel tellus. Suspen",
                "e sit amet varius urna. Proin dictum ultrices urna vitae luctus. Phasellus rutrum ve",
                "enatis augue malesuada sed. Mauris porttitor, leo a viverra scelerisque, libero se",
            };
            var possibleLogTypes = new List<LogType>{
                LogType.Exception,
                LogType.Info,
                LogType.Misc
            };

            return Enumerable.Range(1, fetchCount).Select(x => new Log
            {
                Type = PickRandom(possibleLogTypes),
                Message = PickRandom(possibleLogsMessages)
            }).ToList();
        }

        private T PickRandom<T>(List<T> possibleValues)
        {
            int r = new Random().Next(possibleValues.Count);
            return possibleValues[r];
        }
    }
}
