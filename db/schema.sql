-- Adminer 4.7.6 PostgreSQL dump

\connect "logger";

DROP TABLE IF EXISTS "Settings";
CREATE TABLE "public"."Settings" (
    "Name" character varying(255) NOT NULL,
    CONSTRAINT "Settings_Name" PRIMARY KEY ("Name")
) WITH (oids = false);

INSERT INTO "Settings" ("Name") VALUES
('logCount'),
('autoFetch');

DROP TABLE IF EXISTS "Users";
CREATE TABLE "public"."Users" (
    "Username" character varying(255) NOT NULL,
    CONSTRAINT "Users_Username" PRIMARY KEY ("Username")
) WITH (oids = false);

INSERT INTO "Users" ("Username") VALUES
('Dimalat');

DROP TABLE IF EXISTS "UserSettings";
CREATE TABLE "public"."UserSettings" (
    "SettingName" character varying(255) NOT NULL,
    "Username" character varying(255) NOT NULL,
    "Value" character varying(255) NOT NULL,
    CONSTRAINT "UserSettings_SettingName_fkey" FOREIGN KEY ("SettingName") REFERENCES "Settings"("Name") NOT DEFERRABLE,
    CONSTRAINT "UserSettings_Username_fkey" FOREIGN KEY ("Username") REFERENCES "Users"("Username") NOT DEFERRABLE
) WITH (oids = false);




-- 2021-04-21 18:14:56.830473+00